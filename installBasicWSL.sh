#!/bin/bash
apt update
apt full-upgrade -y
apt autoremove -y
apt install -y git python3 python3-pip python3-venv python3-flask
##azure-cli
curl -sL https://aka.ms/InstallAzureCLIDeb | sudo bash
##dotnet
wget https://packages.microsoft.com/config/ubuntu/22.10/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
dpkg -i packages-microsoft-prod.deb 
rm -rf packages-microsoft-prod.deb 
apt update
apt install -y dotnet-sdk-6.0 aspnetcore-runtime-6.0 dotnet-runtime-6.0
#docker
apt install -y ca-certificates curl gnupg lsb-release
mkdir -p /etc/apt/keyrings
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
echo   "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
apt update
apt install -y docker-ce docker-ce-cli containerd.io docker-compose-plugin
##iptables
echo "Se necessario(caso docker não consiga iniciar por falha do iptables) rode o comando:"
echo "sudo update-alternatives --config iptables"
echo "Selecione a opção iptables-legacy( normalmente a opção 1)"